const crypto = require('crypto');

function genSalt(length) {
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};

function sha512(rawPassword, salt) {
    var hash = crypto.createHmac('sha512', salt);
    hash.update(rawPassword);
    var value = hash.digest('hex');
    return {
        salt:salt,
        password:value
    };
};

function saltHashPassword(rawPassword) {
    var salt = genSalt(16); 
    return sha512(rawPassword, salt);
}

module.exports = {
    saltHashPassword,
    sha512
};