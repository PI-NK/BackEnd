const passport       = require('passport'),
	   session 	     = require('express-session');

const { Usuario } = require('../models/schema')	   

module.exports = () => {
	passport.serializeUser(function(user, done) {
	    done(null, user.id);
	});

	passport.deserializeUser(async (id, done) => {
		await Usuario.query()
		.findOne('id', id)
		.then((user) => { done(null, user); })
		.catch((err) => { done(err, null); });
	});
};