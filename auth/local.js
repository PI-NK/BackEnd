const passport 		 = require('passport'),
	  LocalStrategy  = require('passport-local').Strategy,
	  init           = require('./passport'),
    _auth          = require('./_auth');

const { Usuario } = require('../models/schema');

init();

passport.use(new LocalStrategy({
  usernameField: 'cpf',
  passwordField: 'senha'
},
  async (username, password, done) => {
    console.log('username', username)
    console.log('password', password)
    await Usuario.query()
    .findOne('cpf', username)
    .then(user => {
      console.log('user', user)
      // Usuario nao cadastrado
      if (!user) return done(null, false);

      // Usuario cadastrado
      /// Senha nao cadastrada
      if (user.password === null) {
        //// Cartao SUS inserido nao corresponde a senha cadastrada;
        if (password != user.cardSus) {
          return done(null, false);
      }}
      
      // Senha cadastrada
      if (user.password != null) {
        if (_auth.sha512(password, user.salt).password != user.password) {
          /// Senha incorreta
          return done(null, false);
        } else { 
          // Senha correta
          return done(null, user);
        }
      } 
    })
    .catch((err) => { return done(err); });
}));

module.exports = passport;