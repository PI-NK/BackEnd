require('dotenv').config()

const pg = require('pg')
pg.defaults.ssl = false

module.exports = {
  client: 'pg',
  //connection: process.env.DATABASE_URL
  connection: {
    host : '127.0.0.1',
    user : 'postgres',
    password : 'root',
    database : 'agenda'
  }  
}