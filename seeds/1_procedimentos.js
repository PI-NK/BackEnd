 exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('procedimentos').del()
    .then(function () {
      // Inserts seed entries
      return knex('procedimentos').insert([
        {procedimento: 'saude_idoso', minutos_duracao: 30, dia_semana: 'segunda', horario_inicio: '07:00', horario_fim: '17:00' },
        {procedimento: 'saude_mulher', minutos_duracao: 20, dia_semana: 'terca', horario_inicio: '07:00', horario_fim: '17:00' },
        {procedimento: 'saude_crianca', minutos_duracao: 15, dia_semana: 'quarta', horario_inicio: '07:00', horario_fim: '17:00' },
        {procedimento: 'odontologia', minutos_duracao: 30, dia_semana: 'quinta', horario_inicio: '07:00', horario_fim: '17:00' },
        {procedimento: 'dermatologia', minutos_duracao: 20, dia_semana: 'sexta', horario_inicio: '07:00', horario_fim: '17:00' },                
      ]);
    });
};