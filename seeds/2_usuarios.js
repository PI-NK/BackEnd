exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('usuarios').del()
    .then(function () {
      // Inserts seed entries
      return knex('usuarios').insert([
        {nome: 'Ricardo', cartao_sus: '111.1111.1111.1111', cpf: '111.111.111-11', password: '573c7bb5a6054e1422af49b924e5b93c7964b90218f11880b76f3633ba73121da3a5a7a27336c4925a23ddd28e4763d5f82c8e68cc4119703571818cc938c53e', salt: 'b1ac1a114576cee7'},
        {nome: 'Juniao', cartao_sus: '222.2222.2222.2222', cpf: '222.222.222-22'},
        {nome: 'Alvaro', cartao_sus: '333.3333.3333.3333', cpf: '333.333.333-33'},
        {nome: 'Brito', cartao_sus: '4444', cpf: '444'},
        {nome: 'Lambreta', cartao_sus: '5555', cpf: '555'},
        {nome: 'Leo', cartao_sus: '6666', cpf: '666'},
        {nome: 'Churrasco', cartao_sus: '7777', cpf: '777'}
      ]);
    });
};
