exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('consultas').del()
    .then(function () {
      // Inserts seed entries
      return knex('consultas').insert([
        {horario: new Date(2018,  5, 20, 10, 30, 0), usuario_id: 1, procedimento_id: 1},
        {horario: new Date(2018,  5, 25, 11, 30, 0), usuario_id: 2, procedimento_id: 1},
        {horario: new Date(2018,  6, 12, 12, 30, 0), usuario_id: 3, procedimento_id: 1},
        {horario: new Date(2018, 11, 22,  8, 20, 0), usuario_id: 4, procedimento_id: 2}
      ]);
    });
};