///// Imports /////
const express    	 = require('express'),
	    session      = require('express-session'),
      port       	 = process.env.PORT || 8080,
      rateLimit  	 = require('express-rate-limit'),
      morgan     	 = require('morgan'),
      bodyParser 	 = require('body-parser'),
	    helmet 	 	   = require('helmet'),
	    csurf  	 	   = require('csurf'),
      fs 		 	     = require('fs'),
      csrf 		 	   = require('csrf'),  
      path       	 = require('path'),
      cors         = require('cors'),
	    passport     = require('./auth/local'),            
      routes 	 	   = require('./routes'),
      logger     	 = require('./logger'),
      app        	 = module.exports = express();


///// Configuracao /////

/// Logging de requests ///
app.use(morgan('dev'));

app.use(morgan('dev', {
    skip: function (req, res) {
        return res.statusCode >= 400
    }, stream: fs.createWriteStream(path.join(__dirname, '/logs/requests/successfulRequests.log'), {flags: 'a'})
}));

app.use(morgan('dev', {
    skip: function (req, res) {
        return res.statusCode < 400
    }, stream: fs.createWriteStream(path.join(__dirname, '/logs/requests/failedRequests.log'), {flags: 'a'})
}));


///// Middlewares /////
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({ 
	secret: 'keyboard cat',
	resave: false,
	saveUninitialized: false
}));
app.use(cors({
  origin: 'http://localhost:4200',
  methods: ['POST'],
  allowedHeaders: ['Content-Type'],
  credentials: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/', routes)

/// Seguranca ///
app.use(helmet());
//app.use(csurf());
//app.enable('trust proxy'); // Habilitar ao utilizar Heroku
app.use(new rateLimit({
  windowMs: 20*60*1000, 
  max: 150,
  delayMs: 0
}));

/// Inicializacao ///
app.listen(port, function (err) {
  if (err) { logger.error(err) } else {	
  	logger.info("API Inicializada");
  }
  console.log("Listening in port " + port);
});


/*
https://www.npmjs.com/package/sequelize
https://www.npmjs.com/package/objection
https://vincit.github.io/objection.js/
https://www.npmjs.com/package/express-rate-limit
*/