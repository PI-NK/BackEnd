exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('consultas', table => {
      table.increments('id').primary()
      table.integer('usuario_id').references('usuarios.id').onDelete('CASCADE')
      table.integer('procedimento_id').references('procedimentos.id').onDelete('CASCADE')
      table.dateTime('horario').unique()
    })
    /*
    knex.schema.alterTable('consultas', table => {
      table.unique(['procedimento_id', 'horario'])
    })
    */
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('consultas')
  ])
}