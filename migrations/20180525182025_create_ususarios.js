exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('usuarios', table => {
      table.increments('id').primary()
      table.string('nome')
      table.string('cartao_sus')
      table.string('cpf')
      table.string('password')
      table.string('salt')
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('usuarios')
  ])
}