exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('procedimentos', table => {
      table.increments('id').primary()
      table.string('procedimento')
      table.integer('minutos_duracao')
      table.string('dia_semana')
      table.string('horario_inicio')
      table.string('horario_fim')
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('procedimentos')
  ])
}