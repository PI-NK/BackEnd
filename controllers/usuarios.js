const express 		 = require('express'),
 	  passport       = require('passport'),
	  LocalStrategy  = require('passport-local').Strategy,
	  session 		   = require('express-session');

const { Usuario, Consulta, Procedimento } = require('../models/schema')

const router = express.Router()

router.get('/', async (req, res) => {
  const usuarios = await Usario.query()
  res.json(usuarios)
});

router.post('/login', async (req, res) => {
  const creds = req.body;
  let hashedSaltPass = saltHashPassword(creds.senha);

  passport.use(new LocalStrategy((creds, done) => {
    Usuario.query()
    .where('cpf', creds.cpf)
    .then((user) => {
    	if (!user) return done(null, false);
	    if (saltHashPass.password != user.password) {
	      return done(null, false);
	    } else {
	      return done(null, user);
	    }
    })
  	}
  ))
});

router.use(async(err, req, res, next) => {
    if (err instanceof NotFound) {
        res.status(404).send('Página não encontrada; verifique o endereço digitado.');
    } else {
        next(err);
    }	
})

module.exports = router;