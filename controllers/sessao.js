const express  = require('express'),
	  _auth    = require('../auth/_auth'),
	  passport = require('../auth/local');

const { Usuario, Consulta, Procedimento } = require('../models/schema');

const router = express.Router();


router.post('/checkCpf', async (req, res) => {
  let user = await Usuario.query()
  .findOne({ 'cpf': req.body.cpf })
  .then(user => {
  	if (!user) { res.status(400).send('Usuário não encontrado; favor verifique '); }
  	else if (user && user.password == null) { res.status(200).send('login-sus'); }
  	else if (user && user.password != null) { res.status(200).send('login'); }
  })
});

router.post('/checkSus', async (req, res) => {
  const inputData = req.body;

  await Usuario.query()
  .findOne({ 'cpf': inputData.cpf, 'cartao_sus':inputData.cardSus })
  .then(user => {
  	if (!user) { res.status(400).send('Número do cartão SUS incorreto; favor verifique e tente novamente.'); }
  	if (user) { res.status(200).send('login-senha'); }
  })
});



router.post('/checkSenha', async (req, res) => {
	const inputData = req.body;
  	let hashed = _auth.saltHashPassword(inputData.senha);
    //nome:inputData.nome, cpf:inputData.cpf,  
    await Usuario.query()
    .where('cpf', inputData.cpf)
    .update({password: hashed.password, salt: hashed.salt })
    .then((response) => {
    	passport.authenticate('local', (err, user, info) => {
        if (user) {
          req.logIn(user, function (err) {
            if (err) { res.status(500).send(err); }
            res.status(200).send('agendamento');
          });
        }
    	})(req, res);
    })
    .catch((err => {
    	res.status(500).send(err); }))	
});

router.post('/login', async (req, res) => {
  passport.authenticate('local', (err, user, info) => {
	    if (err) { res.status(500).send(err); }
	    if (!user) { res.status(400).send('Senha incorreta.'); }
	    if (user) {
	      req.logIn(user, function (err) {
	        if (err) { res.status(500).send(err); }
	        res.status(200).send({ url:'agendamento', cpf: user.cpf });
	      });
	    }
	})(req, res);
});

router.post('/logout', async (req, res) => {
	req.logout();
	res.status(200).send('Deslogado com sucesso!');
});


router.post('/teste', async (req, res) => {
	if (req.isAuthenticated()) {
		res.status(200).send('Deu certo!')
	} else {
		res.status(400).send('Inicie uma sessao para acessar esta página.')
	}
});

module.exports = router;