const express = require('express');
      logger  = require('../logger');

const { Usuario, Consulta, Procedimento } = require('../models/schema');

const router = express.Router()

router.post('/todos', async (req, res) => {
  const procedimentos = await Procedimento.query()
  res.json(procedimentos)
});

module.exports = router;