const express = require('express');
      logger  = require('../logger');

const { Usuario, Consulta, Procedimento } = require('../models/schema');

const router = express.Router()

router.get('/', async (req, res) => {
  const consultas = await Consulta.query()
  res.json(consultas)
});

router.post('/agendadas', async (req, res) => { 
  /*
  let consultas = await Consulta.query().select('horario').where('procedimento_id', (
  	await Procedimento.query().select('id').findOne('procedimento', req.body.procedimento)).id
  )
  */
  if (req.isAuthenticated()) {
    let consultas = await Consulta
    .query()
    .select(['horario', 'usuario_id', 'procedimento_id'])

    for (let i=0; i < consultas.length; i++) {
    	if (consultas[i].usuario_id === req.user.id) {
    		consultas[i].proprio = true;
    	}
    	delete consultas[i].usuario_id;
    }

    res.send(consultas)
  } else {
    res.status(400).send("Inicie a sessão para acessar esta página")
  }
});

router.post('/agendar', checkAuthentication,  async (req, res) => {
	let userInput = req.body;
	
	try {
		let procedimento = await Procedimento
		.query()
		.select('procedimentos.id')
		.findOne('procedimento', userInput.procedimento)

		await Consulta
		.query()
		.insert({
			'usuario_id': req.user.id,
			'procedimento_id': procedimento['id'],
			'horario': new Date(userInput.horario)
		});

		res.status(200).send('Consulta inserida com sucesso');
	} catch(err) {
		logger.error({ err: err	});
		res.status(400).send('Ocorreu um erro ao inserir a consulta; talvez o horário especificado tenha sido selecionado durante o processamento do pedido.');
	}
});

function checkAuthentication(req,res,next) {
    if(req.isAuthenticated()) {
        next();
    } else {
        res.redirect("/login");
    }
}

module.exports = router;