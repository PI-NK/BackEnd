const express  		 = require('express'),
	  router  		 = express.Router({ mergeParams: true }),
	  procedimentos  = require('./controllers/procedimentos'),
	  consultas  	 = require('./controllers/consultas'),
	  usuarios   	 = require('./controllers/usuarios'),
	  sessao 		 = require('./controllers/sessao');

router.use('/procedimentos', procedimentos);
router.use('/consultas', consultas);
router.use('/usuarios', usuarios);
router.use('/sessao', sessao);

module.exports = router;