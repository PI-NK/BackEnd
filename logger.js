const bunyan = require("bunyan"),
      path   = require('path');

const logger = bunyan.createLogger({
    name: "bunyanLogger",
    serializers: bunyan.stdSerializers,
    streams: [
        {
            level: "info",
            path: path.join(__dirname, './logs/local/access.log')
        },
        {
        	level: "error",
        	path: path.join(__dirname, './logs/local/errors.log')
        }
    ]
});

module.exports = logger;