const Knex = require('knex')
const connection = require('../knexfile')
const { Model } = require('objection')

const knexConnection = Knex(connection)
Model.knex(knexConnection)

class Usuario extends Model {
  static get tableName () {
    return 'usuarios'
  }

  static get relationMappings () {
    return {
      procedimento: {
        relation: Model.ManyToManyRelation,
        modelClass: Procedimento,
        join: {
          from: 'usuarios.id',
          through: {
          	modelClass: Consulta,
          	from: 'consultas.usuario_id',
          	to: 'consultas.procedimento_id',
            extra: ['horario']
          },
          to: 'procedimentos.id'
        }
      }
    }
  }
}


class Consulta extends Model {
  static get tableName () {
    return 'consultas'
  }
}


class Procedimento extends Model {
  static get tableName () {
    return 'procedimentos'
  }

  static get relationMappings () {
    return {
      usuario: {
        relation: Model.ManyToManyRelation,
        modelClass: Usuario,
        join: {
          from: 'procedimentos.id',
          through: {
          	modelClass: Consulta,
          	from: 'consultas.procedimento_id',
          	to: 'consultas.usuario_id',
            extra: ['horario']
          },
          to: 'usuarios.id'
        }
      }
    }
  }
}

module.exports = { Usuario, Consulta, Procedimento }